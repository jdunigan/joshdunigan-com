+++
title = "36"
+++

Thought does not come from nothing. I think only some naive Christian idealist could hold that position. Say it is no different than any other cultural artifact, a product of desire, sublimation.
Philosophy then is about control. Controlling the meaning of words, reality, signs and symbols, what reason is, etc.
"Actually, when we think of consciousness this way, my entire theory is entirely consistent."
A bunch of house of cards.

