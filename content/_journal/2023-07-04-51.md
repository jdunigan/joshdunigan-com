+++
title = "51"
+++

When you are inside of a conversation, the diversity of thought can mislead you into thinking that you are being truly holistic. For example, the history of Western philosophy, whatever that even means. Taking people like Kant or Hegel, they appear to many to be such holistic thinkers, gracefully grappling with all sorts of competing schools of thought.
But no conversation, or even disagreement, can happen without some prior agreement. For a conversation at some event, the agreement is that the two people are at the same place at the same time, it already says quite a bit about them.
For philosophy, even if you have empiricism vs idealism, etc., there always exists a shared perspective or value even if not stated explicitly. For Kant and Hegel, it may be someone who thinks through philosophy with a Christian bent. It might be a shared heritage of something like Greek and Roman thought. It may be a type of writing style.
We have to be on guard against these things in philosophy, otherwise you end up saying "this is philosophy" when in fact it is just one type.
