+++
title = "29"
+++

It is no wonder Freud found a goldmine of content in the Oedipal drama, transference, repression, repetition, censorship, etc., a whole host of concepts that center around authenticity, how we inevitably end up actors in some play we are not even aware of, and when we are aware, not knowing how to escape the dream or nightmare of a play.
