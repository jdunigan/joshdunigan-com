+++
title = "41"
+++

Talking is a pleasurable activity in itself that more than ever might feel out of reach for many.
