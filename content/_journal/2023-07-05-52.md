+++
title = "52"
+++

Kongzi's focus on rituals to many, of his time or not, of his people or not, to be misplacing what true virtue comes from. One could write a similar work as Kant did called _Ritual within the Boundaries of Mere Reason_. Something that is missed in this hypothetical defense of Kongzi is that Kongzi knew that we cannot act with pure intention, our intention gets mapped into the world of signs, symbols, habit, etc.
Virtue has to be acted out, you need an audience. It needs to be clearly expressed. No one can question Kongzi's obedience to his lord when he does not even wait for the cart to pull up when called upon, he starts walking and the cart will catch up to him.
No rituals and going through the motions of rituals would be the same to Kongzi, it would show a lack of virtue. Rituals make the man, Kongzi chose the rituals of the greatest men of he could find.
