+++
title = "About"

description = "About"

# Used to sort pages by "date", "update_date", "title", "title_bytes", "weight", "slug" or "none". See below for more information.
sort_by = "none"

# If set to "true", the section homepage is rendered.
# Useful when the section is used to organize pages (not used directly).
render = true

template = "page.html"
# Your own data.
[extra]
+++

## About me

I studied computer science, math, and philosophy at the University of Illinois at Urbana-Champaign, receiving a degree in philosophy.
I currently work as an engineering manager at Justworks. I live in New York City with my beautiful wife Melanie, where I spend much of my time outside work with her or reading and writing.

<img src="/img/me-and-mel.jpeg" alt="Mel and I" width="50%" />
