+++
title = "My Library"

description = "The physical books I own"

# Used to sort pages by "date", "update_date", "title", "title_bytes", "weight", "slug" or "none". See below for more information.
sort_by = "none"

# If set to "true", the section homepage is rendered.
# Useful when the section is used to organize pages (not used directly).
render = true

template = "page.html"
# Your own data.
[extra]
+++

I recently digitized my library using Libib, [which you can see here. ](https://www.libib.com/u/joshdunigan-library)
