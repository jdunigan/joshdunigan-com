+++
title = "My Library"

description = "The physical books I own"

# Used to sort pages by "date", "update_date", "title", "title_bytes", "weight", "slug" or "none". See below for more information.
sort_by = "none"

# If set to "true", the section homepage is rendered.
# Useful when the section is used to organize pages (not used directly).
render = true

template = "page.html"
# Your own data.
[extra]
+++

## My Library

Not including journals, magazines, and digital copies of books. Does not include art coffee table books. Some collections have been condensed into one item.


### Small side shelf

| Title                                                   |                           Author(s)                            | Read  |
| :------------------------------------------------------ | :------------------------------------------------------------: | :---: |
| Subject Lessons                                         |                        Sbriglia & Zizek                        |  No   |
| Reading Capital: The Complete Edition                   |      Althusser, Balibar, Establet, Macherey, and Ranciere      |  No   |
| Cogito and the Unconscious                              |                           ed. Zizek                            |  No   |
| Essays and Reviews: 1959-2002                           |                        Bernard Williams                        |  Yes  |
| Japanese philosophy book for kids                       |                            not sure                            |  No   |
| The Terrorism Reader                                    |                         Walter Laqueur                         |  No   |
| To Shape a New World                                    |                Tommie Shelby, Brandon M. Terry                 |  Yes  |
| History of Political Philosophy                         |                ed. Leo Strauss, Joseph Cropsey                 |  No   |
| The Critical Spirit: Essays in Honor of Herbert Marcuse |             ed. Kurt H. Wolf, Barrington Moore Jr.             |  No   |
| Dialectic of Enlightenment                              |                     Horkheimer and Adorno                      |  No   |
| Current Continental Theory and Modern Philosophy        |                             Daniel                             |  No   |
| Aesthetics and Politics                                 |          Adorno, Benjamin, Bloch, Brecht, and Lukacs           |  No   |
| Towards a New Manifesto                                 |                       Adorno, Horkheimer                       |  No   |
| Alasdair MacIntyre's Engagement with Marxism            |               ed. Paul Blackledge, Neil Davidson               |  No   |
| Limits of Analysis                                      |                             Rosen                              |  No   |
| Retreat from Truth                                      |                         G. M. G. Mure                          |  No   |
| The Hegel Reader                                        |                          ed. Houlgate                          |  No   |
| The Dawn of Everything                                  |                  David Graeber, David Wengrow                  |  Yes  |
| Modern Philosophy: An Anthology of Primary Sources      |                 ed. Roger Ariew, Eric Watkins                  | Some  |
| Nagarjuna                                               |                          Chr. Lindner                          |  No   |
| Hatred of Capitalism                                    |                      ed. Kraus, Lotringer                      |  No   |
| Robert Brandom                                          |              ed. Bernd Prien, David P. Schweikard              |  No   |
| The Dialectic of Liberation                             | Stokely Carmichael, David Cooper, R. D. Laing, Herbert Marcuse |  No   |
| Post-Analytic Philosophy                                |                 ed. John Rajchman, Cornel West                 |  Yes  |
| German Quickly                                          |                           Peter Lang                           |  No   |
| The Times                                               |                         Adam Nagourney                         |  Yes  |
| The Bible                                               |                           King James                           |  No   |
| Philosophy in History                                   |                   Rorty, Schneewind, Skinner                   |  Yes  |
| Contemporary Marxist Theory                             |        ed. Pendakis, Diamanti, Brown, Robinson, Szeman         |  No   |
| Philosophy of Mind                                      |                           John Heil                            |  Yes  |
| The Cambridge Companion to Marx                         |                           Cambridge                            |  No   |
| Marx's Capital 150 Years Later                          |                       ed. Marcello Musto                       |  No   |
| Palestine: A Socialist Introduction                     |                   Sumaya Awad and Brian Bean                   |  No   |
| The Literary Absolute                                   |                     Lacoue-Labarthe, Nancy                     |  No   |
| Inside the Critics Circle                               |                             Chong                              |  No   |
| Selected Tweets                                         |                         Lin, Gonzalez                          | Some  |
| The French Right: From De Maistre to Maurras            |                      ed. J. S. McClelland                      |  No   |
| I Ching Wisdom II                                       |                             Wu Wei                             |  No   |
| Kant's Final Synthesis                                  |                            Forster                             |  No   |
| Kafka's Other Trial                                     |                         Elias Canetti                          |  No   |
| A Philosophy of Walking                                 |                         Frederic Gros                          |  Yes  |
| The Rigor of Angels                                     |                       William Eggington                        |  No   |
| When We Cease To Understand The World                   |                        Benjamin Labatut                        |  No   |
| Kantian Nonconceptualism                                |                      ed. Dennis Schulting                      |  No   |
| Christendom                                             |                         Peter Heather                          |  No   |
| Palestinian Walks                                       |                         Raja Shehadeh                          |  Yes  |
| What Does Israel Fear From Palestine?                   |                         Raja Shehadeh                          |  No   |
| Letters to My Palestinian Neighbor                      |                       Yossi Klein Halevi                       |  Yes  |
| The Hundreds Years' War on Palestine                    |                         Rashid Khalidi                         |  No   |
| The Kingdom and the Power                               |                           Gay Talese                           |  No   |
| A Hitch in Time                                         |                      Christopher Hitchens                      |  Yes  |


### Top of big bookcase
| Title                                                      |       Author(s)        | Read  |
| :--------------------------------------------------------- | :--------------------: | :---: |
| In Search of Lost Time                                     |     Marcel Proust      |  Yes  |
| Letters of Marcel Proust                                   |     Marcel Proust      |  No   |
| Marcel Proust                                              |   William C. Carter    |  No   |
| Proust's Binoculars                                        |     Roger Shattuck     |  No   |
| Proust A-Z                                                 |      Lydia Davis       |  No   |
| Proust and Signs                                           |     Gilles Deleuze     |  No   |
| The World According to Proust                              |         Landy          |  No   |
| Lost Time: Lectures on Proust in a Soviet Prison Camp      |     Jozef Czapski      |  No   |
| Proust's Way                                               |     Roger Shattuck     |  No   |
| Monsieur Proust                                            |    Celeste Albaret     |  No   |
| Proustian Uncertanties                                     |    Saul Friedlander    |  Yes  |
| The Mysterious Correspondent                               |     Marcel Proust      |  No   |
| Pleasures and Regrets                                      |     Marcel Proust      |  No   |
| Proust and Three Dialogues with Georges Duthut             |     Samuel Beckett     | Some  |
| The Magic Mountain                                         |      Thomas Mann       |  Yes  |
| Doctor Faustus                                             |      Thomas Mann       |  No   |
| The Holy Sinner                                            |      Thomas Mann       |  No   |
| Death in Venice and Other Tales                            |      Thomas Mann       |  No   |
| Essays Two                                                 |      Lydia Davis       |  No   |
| The Setting Sun                                            |      Osamu Dazai       |  No   |
| No Longer Human                                            |      Osamu Dazai       |  Yes  |
| The Remainder                                              |      Tom McCarthy      |  Yes  |
| The Trial                                                  |         Kafka          |  Yes  |
| The Metamorphoses and Other Stories                        |         Kafka          |  Yes  |
| Parables                                                   |         Kafka          |  No   |
| Amerika                                                    |         Kafka          |  No   |
| Diaries                                                    |         Kafka          | Some  |
| The Complete Stories                                       |         Kafka          |  No   |
| The Beautiful and the Damned                               |       Fitzgerald       |  No   |
| Jude the Obscure                                           |      Thomas Hardy      |  No   |
| Faust                                                      |         Goethe         |  No   |
| An Unquiet Mind                                            |  Kay Redfield Jamison  |  Yes  |
| The Satanic Verses                                         |     Salman Rushdie     | Some  |
| Vertigo                                                    |      W. G. Sebald      |  No   |
| In Cold Blood                                              |         Capote         |  No   |
| Trauma and Recovery                                        |         Herman         |  No   |
| The Rings of Saturn                                        |      W. G. Sebald      |  No   |
| Blue Nights                                                |      Joan Didion       |  No   |
| The Idiot                                                  |   Fyodor Dostoevsky    |  No   |
| The Victim                                                 |      Saul Bellow       |  No   |
| The Complete Poems                                         |     Philip Larkin      |  No   |
| The History of Sexuality: An Introduction                  |        Foucault        |  No   |
| Blood Dark                                                 |     Louis Guilloux     |  No   |
| Slaughterhouse-Five                                        |     Kurt Vonnegut      |  No   |
| Gravity's Rainbow                                          |     Thomas Pynchon     |  No   |
| The Crying of Lot 49                                       |     Thomas Pynchon     |  No   |
| The Aleph and Other Stories                                |         Borges         |  No   |
| Explosion in a Cathedral                                   |    Alejo Carpentier    |  No   |
| Ulysses                                                    |         Joyce          |  No   |
| Cat's Cradle                                               |     Kurt Vonnegut      |  No   |
| The Short Stories of Dostoevsky                            |       Dostoevsky       |  No   |
| The Brother's Karamazov                                    |   Fyodor Dostoevsky    | Some  |
| A Portrait of the Artist as a Young Man                    |         Joyce          |  No   |
| Terminal Boredom                                           |      Izumi Suzuki      |  No   |
| Siddhartha                                                 |      Herman Hesse      |  No   |
| Blood Meridian                                             |        McCarthy        |  No   |
| The Melancholy of Resistance                               |  Laszlo Krasznahorkai  |  No   |
| The Death of Jesus                                         |     J. M. Coetzee      |  No   |
| Disgrace                                                   |     J. M. Coetzee      |  No   |
| The Schooldays of Jesus                                    |     J. M. Coetzee      |  No   |
| The Childhood of Jesus                                     |     J. M. Coetzee      |  No   |
| Moby-Dick or, The Whale                                    |    Herman Melville     |  Yes  |
| The Loser                                                  |    Thomas Bernhard     |  No   |
| I Novels                                                   |    Samuell Beckett     |  No   |
| II Novels                                                  |    Samuell Beckett     | Some  |
| Dramatic Works                                             |    Samuell Beckett     |  No   |
| Poems, Short Fiction, Criticism                            |    Samuell Beckett     |  No   |
| The Complete Poems                                         |       John Keats       |  No   |
| The Selected Poetry of Ranier Marie Rilke                  |         Rilke          |  No   |
| Haiku                                                      |      R. H. Blyth       |  No   |
| Bright Moon, White Clouds: Selected Poems of Li Po         |         Li Po          |  No   |
| Leaves of Grass (Deathbed Edition)                         |      Walt Whitman      | Some  |
| Collected Works                                            |     Anton Chekhov      |  No   |
| The Flame                                                  |     Leonard Cohen      |  No   |
| Letters to a Young Poet                                    |   Ranier Marie Rilke   |  No   |
| Letters to a Young Poet                                    |   Ranier Marie Rilke   |  No   |
| Rimbaud Complete Poetry and Prose                          |     Arthur Rimbaud     | Some  |
| Spring and All                                             |     W. C. Williams     |  No   |
| Parables for the Theatre                                   |     Bertolt Brecht     |  No   |
| Selected Poems and Fragments                               |       Holderlin        |  No   |
| Dark Night of the Soul                                     | St. John of the Cross  |  No   |
| An Episode in the Life of a Landscape Painter              |       Cesar Aira       |  No   |
| The Classic Noh Theater                                    |    Pound, Fenellosa    |  No   |
| The Narrow Road to the Deep North and Other Travel Stories |         Basho          |  No   |
| Stories of Mr. Keuner                                      |         Brecht         |  No   |
| Night Sky with Exit Wounds                                 |      Ocean Vuong       |  Yes  |
| Myths                                                      |      Gary Snyder       |  No   |
| Japanese Death Poems                                       |     Yoel, Hoffmann     |  No   |
| Life and Work                                              |    Andrey Tarkovsky    |  Yes  |
| A Hundred Years of Japanese Film                           |         Richie         |  No   |
| The Detroit Printing Co-Op                                 |         Aubert         |  No   |
| Bergman on Bergman                                         | Bjorkmann, Manns, Sima |  No   |
| The Noh Drama                                              |         Tuttle         |  No   |
| 127-749                                                    |    David A. Grinage    |  Yes  |
| The Tale of Genji                                          |    Murasaki Shikibu    |  No   |
| Sculpting in Time                                          |       Tarkovsky        |  Yes  |
| Poems 1913-1956                                            |         Brecht         | Some  |
| Behind My Eyes                                             |      Li-Young Lee      | Some  |
| First Sunrise                                              | Premier Soleil Levant  |  Yes  |
| Collected Haiku of Yosa Buson                              |       Yosa Buson       |  No   |
| Les Fleurs du Mal                                          |   Charles Baudelaire   |  No   |
| Our Death                                                  |      Sean Bonney       |  Yes  |
| The World of Jia Zhangke                                   |   Jean-Michel Frodon   |  No   |
| The Emergence of Social Space                              |          Ross          |  No   |
| Autobiography of Red                                       |      Anne Carson       |  No   |
| The Essential Haiku                                        |    tr. Robert Hass     |  Yes  |
| Lucky Jim                                                  |     Kingsley Amis      |  No   |

### Big bookcase
| Title                                                                             |             Author(s)              | Read  |
| :-------------------------------------------------------------------------------- | :--------------------------------: | :---: |
| Aesthetics                                                                        |               Adorno               |  No   |
| Essays on Music                                                                   |               Adorno               | Some  |
| Aesthetic Theory                                                                  |               Adorno               |  No   |
| The Stars Down to Earth                                                           |               Adorno               |  No   |
| In Search of Wagner                                                               |               Adorno               |  No   |
| Mahler                                                                            |               Adorno               |  No   |
| Minima Moralia                                                                    |               Adorno               |  Yes  |
| The Culture Industry                                                              |               Adorno               |  Yes  |
| Hegel: Three Studies                                                              |               Adorno               |  No   |
| Negative Dialectics                                                               |               Adorno               |  No   |
| Manifest Reality                                                                  |               Allais               |  Yes  |
| Kant's Conception of Freedom                                                      |              Allison               |  Yes  |
| Kant's Transcendental Idealism                                                    |              Allison               |  Yes  |
| Kant's Transcendental Deduction                                                   |              Allison               |  No   |
| Essays on Kant                                                                    |              Allison               | Some  |
| The Spectre of Hegel                                                              |             Althusser              |  No   |
| Lectures on Rousseau                                                              |             Althusser              |  No   |
| For Marx                                                                          |             Althusser              |  No   |
| On Ideology                                                                       |             Althusser              |  No   |
| Kant and the Fate of Autonomy                                                     |              Ameriks               |  Yes  |
| The Antinomies of Antonio Gramsci                                                 |           Perry Anderson           |  No   |
| An Introduction to Wittgenstein's Tractatus                                       |          G.E.M. Anscombe           |  No   |
| The Human Condition                                                               |               Arendt               |  Yes  |
| On Violence                                                                       |               Arendt               |  No   |
| The Origins of Totalitarianism                                                    |               Arendt               |  Yes  |
| The Life of the Mind                                                              |               Arendt               | Some  |
| Collected Works of Aristotle                                                      |             ed. Barnes             | Some  |
| Confessions                                                                       |          Saint Augustine           |  No   |
| Being and Event                                                                   |               Badiou               |  No   |
| Ethics: An Essay on the Understanding of Evil                                     |               Badiou               |  No   |
| In Praise of Love                                                                 |               Badiou               |  Yes  |
| Pocket Pantheon                                                                   |               Badiou               |  No   |
| The age of the Poets                                                              |               Badiou               |  No   |
| Cinema                                                                            |               Badiou               |  No   |
| Metapolitics                                                                      |               Badiou               |  No   |
| Rhapsody for the Theatre                                                          |               Badiou               |  No   |
| The Philosophy of Marx                                                            |          Etienne Balibar           |  No   |
| The Bataille Reader                                                               |              Battaile              | Some  |
| The Sacred Conspiracy                                                             |              Bataille              |  Yes  |
| The Mandarins                                                                     |              Beauvoir              |  No   |
| The Second Sex                                                                    |              Beauvoir              |  No   |
| The Ehics of Ambiguity                                                            |              Beauvoir              |  No   |
| Presupposition and Assertion in Dynamic Semantics                                 |               Beaver               |  Yes  |
| A Commentary on Kant's Critique of Practical Reason                               |                Beck                |  No   |
| After Hegel                                                                       |               Beiser               |  No   |
| German Idealism                                                                   |               Beiser               |  No   |
| Hegel                                                                             |               Beiser               |  No   |
| The Fate of Reason                                                                |               Beiser               |  No   |
| Kant's Dialectic                                                                  |               Bennet               |  No   |
| Kant's Analytic                                                                   |               Bennet               |  No   |
| Illuminations                                                                     |              Benjamin              |  Yes  |
| Orgin of the German Trauerspiel                                                   |              Benjamin              |  No   |
| Reflections                                                                       |              Benjamin              |  No   |
| Sonnets                                                                           |              Benjamin              |  No   |
| Early Writings: 1910-1917                                                         |              Benjamin              |  No   |
| One-Way Street                                                                    |              Benjamin              |  No   |
| The Work of Art in the Age of Its Technology Reproducibility                      |              Benjamin              |  No   |
| The Arcades Project                                                               |              Benjamin              |  No   |
| The Storyteller Essays                                                            |              Benjamin              |  No   |
| The Correspondence of Walter Benjamin                                             |              Benjamin              | Some  |
| On Karl Marx                                                                      |               Block                |  No   |
| Logic and the Limits of Philosophy in Kant and Hegel                              |               Bohnet               |  No   |
| The Collected Works of W.E.B. Du Bois                                             |               Oxford               |  No   |
| Black Reconstruction in America                                                   |              Du Bois               | Some  |
| Manet                                                                             |              Bourdieu              |  No   |
| Appearance and Reality                                                            |              Bradley               |  No   |
| The Principles of Logic                                                           |              Bradley               |  No   |
| A Spirit of Trust                                                                 |              Brandom               |  Yes  |
| Reason in Philosophy                                                              |              Brandom               |  No   |
| Kant: An Introduction                                                             |               Broad                |  No   |
| A Philosophical Enquiry into the Sublime and Beautiful                            |               Burke                |  No   |
| Reflections on the Revolution in France                                           |               Burke                |  No   |
| Mythic Worlds, Modern Words                                                       |              Campbell              |  No   |
| The Power of Myth                                                                 |              Campbell              |  No   |
| The Hero with a Thousand Faces                                                    |              Campbell              |  No   |
| If Not, Winter: Fragments of Sappho                                               |            Anne Carson             |  No   |
| Eros the Bittersweet                                                              |               Carson               |  Yes  |
| The Philosophy of Symbolic Forms                                                  |              Cassirer              |  No   |
| The Logical Structure of the World                                                |               Carnap               | Some  |
| Philosophy of Mind                                                                |              Chalmers              |  No   |
| Karl Marx's Theory of History                                                     |               Cohen                |  No   |
| Lectures on the History of Moral and Political Philosophy                         |               Cohen                |  No   |
| Marxism and Hegel                                                                 |           Lucio Colletti           |  No   |
| Essay on Metaphysics                                                              |            Collingwood             |  No   |
| The Analects                                                                      |             Confucius              |  Yes  |
| Caste, Class, and Race                                                            |                Cox                 |  No   |
| Freud: The Making of an Illusion                                                  |               Crews                |  No   |
| Are Prisons Obsolete?                                                             |               Davis                |  Yes  |
| Women, Race, Class                                                                |               Davis                |  No   |
| Old Gods, New Enigmas                                                             |             Mike Davis             |  No   |
| Speculation                                                                       |                Daly                |  No   |
| The Philosophical Writings of Descartes                                           |             Descartes              | Some  |
| Leibniz's New Essays on Human Understanding                                       |               Dewey                |  No   |
| Art as Experience                                                                 |               Dewey                |  No   |
| Individualism Old and New                                                         |               Dewey                |  No   |
| Democracy as Education                                                            |               Dewey                |  No   |
| Psychology                                                                        |               Dewey                |  No   |
| Rameau's Nephew and D'Alembert's Dream                                            |              Diderot               |  No   |
| What Computers Still Can't Do                                                     |              Dreyfus               |  No   |
| Kant after Duchamp                                                                |              de Duve               |  No   |
| Literary Theory                                                                   |              Eagleton              |  No   |
| Why Marx Was Right                                                                |              Eagleton              |  No   |
| Reason, Faith, and Revolution                                                     |              Eagleton              |  No   |
| The Murder of Professor Schlick                                                   |              Edmonds               |  No   |
| Time of the Magicians                                                             |            Eilenberger             |  No   |
| Outlines of Skepticism                                                            |          Sextus Empiricus          |  No   |
| The Art of Happiness                                                              |              Epicurus              |  No   |
| The Wretched of the Earth                                                         |               Fanon                |  No   |
| Black Skin, White Masks                                                           |               Fanon                |  No   |
| Hegel's Social Theory                                                             |              Farneth               |  No   |
| Conservatism                                                                      |              Fawcett               | Some  |
| Thinking and the I                                                                |              Ferrarin              |  No   |
| Against Method                                                                    |             Feyerabend             |  No   |
| Conquest of Abundancce                                                            |             Feyerabend             |  No   |
| Democracy: Ancient & Modern                                                       |               Finley               |  No   |
| Foundations of the Entire Wissenschaftlehre                                       |               Fichte               |  Yes  |
| Contribution to the Correction of the Public's Judgments on the French Revolution |               Fichte               |  Yes  |
| Introduction to the Wissenschaftlehre                                             |               Fichte               |  No   |
| K-Punk                                                                            |            Mark Fisher             |  No   |
| In the Shadow of Justice                                                          |             Forrester              |  No   |
| The Twenty-Five Years of Philosophy                                               |              Forster               |  No   |
| Rousseau, Nietzsche, and the Image of the Human                                   |               Franco               |  No   |
| Pedagogy of the Oppressed                                                         |               Friere               |  No   |
| All or Nothing                                                                    |               Franks               |  No   |
| Kant and the Exact Sciences                                                       |              Friedman              |  No   |
| Studies on Hysteria                                                               |               Freud                |  No   |
| A General Introduction to Psychoanalysis                                          |               Freud                |  No   |
| Three Essays on Sexuality                                                         |               Freud                |  Yes  |
| Civilization and its Discontents                                                  |               Freud                |  Yes  |
| The Future of an Illusion                                                         |               Freud                |  No   |
| Jokes and Their Relation to the Unconscious                                       |               Freud                |  No   |
| Group Psychology and the Analysis of the Ego                                      |               Freud                |  No   |
| Introductory Lectures on Psychoanalysis                                           |               Freud                |  Yes  |
| Beyond the Pleasure Principle                                                     |               Freud                |  Yes  |
| The Ego and the Id                                                                |               Freud                |  Yes  |
| The Interpretation of Dreams                                                      |               Freud                | Some  |
| The Uncanny                                                                       |               Freud                |  No   |
| The Individual and His Society                                                    |              Kardiner              |  No   |
| Traumatic Neuroses of War                                                         |              Kardiner              |  Yes  |
| Anatomy of Criticism                                                              |                Frye                |  No   |
| Hegel's Dialectic                                                                 |              Gadamer               |  No   |
| Robespierre                                                                       |              Gauchet               |  No   |
| Freud                                                                             |             Peter Gay              |  No   |
| Not Thinking Like a Liberal                                                       |               Geuss                |  No   |
| The Idea of a Critical Theory                                                     |               Geuss                |  Yes  |
| The Normativity of Nature                                                         |              Ginsburg              |  No   |
| Immanuel Kant                                                                     |          Lucien Goldmann           |  Yes  |
| Fragments of an Anarchist Anthropology                                            |              Graeber               |  No   |
| Prison Diaries Vol 1                                                              |              Gramsci               |  No   |
| Kant and the Claims of Taste                                                      |               Guyer                |  No   |
| Kant on Freedom, Law, and Happiness                                               |               Guyer                |  No   |
| Knowledge and Human Interests                                                     |              Habermas              |  No   |
| The Postnational Constitution                                                     |              Habermas              |  No   |
| This Life                                                                         |              Hagglund              |  No   |
| Selected Writings on Marxism                                                      |                Hall                |  No   |
| Between Heidegger and Novalis                                                     |               Hanley               |  No   |
| The Limits of Capital                                                             |               Harvey               |  No   |
| Companion to Marx's Capital                                                       |               Harvey               |  No   |
| The Enigma of Capital                                                             |               Harvey               |  No   |
| Hegel's Ladder                                                                    |            H. S. Harris            |  No   |
| Summer Meditations                                                                |               Havel                |  Yes  |
| Elements of the Philosophy of Right                                               |               Hegel                |  Yes  |
| The Science of Logic                                                              |               Hegel                |  Yes  |
| The Phenomenology of Spirit                                                       |               Hegel                |  Yes  |
| Political Writings                                                                |               Hegel                |  Yes  |
| Hegel On Haman                                                                    |               Hegel                |  No   |
| Early Theological Writings                                                        |               Hegel                |  No   |
| Aesthetics                                                                        |               Hegel                |  No   |
| Philosphy of Nature                                                               |               Hegel                |  No   |
| The Difference Between Fichte's and Schelling's System of Philosophy              |               Hegel                |  No   |
| Lectures on the Philosophy of Religion                                            |               Hegel                |  No   |
| Philosophy of Mind                                                                |               Hegel                |  No   |
| Lectures on the History of Philosophy                                             |               Hegel                |  No   |
| Introductory Lectures on Aesthetics                                               |               Hegel                |  No   |
| Werke 3                                                                           |               Hegel                |  No   |
| Positive Nihilism                                                                 |           Hartmut Lange            |  No   |
| The Cambridge Companion to Heidegger                                              |             Cambridge              |  Yes  |
| Heidegger's Hut                                                                   |             Adam Sharr             |  No   |
| The Metaphysics of German Idealism                                                |             Heidegger              |  No   |
| Being and Time                                                                    |             Heidegger              |  Yes  |
| Heraclitus                                                                        |             Heidegger              |  Yes  |
| Heraclitus Seminar                                                                |             Heidegger              |  Yes  |
| Phenomenological Interpretation of Kant's *Critique of Pure Reason*               |             Heidegger              |  Yes  |
| Four Seminar                                                                      |             Heidegger              |  Yes  |
| Contributions to Philosophy                                                       |             Heidegger              |  No   |
| Introduction to Metaphysics                                                       |             Heidegger              |  Yes  |
| Ontology: The Hermeneutics of Facticity                                           |             Heidegger              |  No   |
| Kant and the Problem of Metaphysics                                               |             Heidegger              |  Yes  |
| The Essence of Truth                                                              |             Heidegger              |  No   |
| Nietzsche Lectures                                                                |             Heidegger              |  No   |
| Poetry, Language, Truth                                                           |             Heidegger              |  Yes  |
| Basic Writings                                                                    |             Heidegger              |  Yes  |
| Heidegger's Changing Destinies                                                    |          Guillaume Payen           |  Yes  |
| The Politics of Being                                                             |               Wolin                | Some  |
| Heidegger's Children                                                              |               Wolin                |  No   |
| Heidegger in Ruins                                                                |               Wolin                |  No   |
| Heidegger's Interpretation of Kant                                                |              Lambeth               |  No   |
| How to Read Marx's Capital                                                        |          Michael Heinrich          |  No   |
| An Introduction to the Three Volumes of Karl Marx's Capital                       |          Michael Heinrich          |  Yes  |
| Between Kant and Hegel                                                            |              Henrich               |  No   |
| The practice of Moral Judgement                                                   |               Herman               |  No   |
| Moral Literacy                                                                    |               Herman               |  No   |
| Existence: A Story                                                                |               Hinton               |  No   |
| Lost In Thought                                                                   |                Hitz                |  Yes  |
| Leviathan                                                                         |               Hobbes               |  Yes  |
| Immanuel Kant                                                                     |               Hoffe                |  No   |
| Reification                                                                       |              Honneth               |  No   |
| Pathologies of Individual Freedom                                                 |              Honneth               |  No   |
| From Hegel to Marx                                                                |                Hook                |  No   |
| Leibniz Critical and Interpretive Essays                                          |             ed. Hooker             |  No   |
| An Introduction to Hegel                                                          |              Houlgate              |  No   |
| The Opening of Hegel's Logic                                                      |              Houlgate              |  No   |
| Hegel and the Arts                                                                |              Houlgate              |  No   |
| Hegel on Being Vol. 1                                                             |              Houlgate              |  Yes  |
| Hegel on Being Vol. 2                                                             |              Houlgate              |  No   |
| Kant's NonIdeal Theory of Politics                                                |           Huseyinzadegan           |  No   |
| Genesis & Structure of Hegel's *Phenomenology of Spirit*                          |             Hyppolite              |  No   |
| Rousseau and German Idealism                                                      |               James                |  No   |
| The Years of Theory                                                               |              Jameson               |  No   |
| The Hegel Variations                                                              |              Jameson               |  No   |
| Brecht and Method                                                                 |              Jameson               |  No   |
| The Ancients and the Postmoderns                                                  |              Jameson               |  No   |
| Late Marxism                                                                      |              Jameson               |  No   |
| The Benjamin Files                                                                |              Jameson               |  No   |
| Valences of the Dialectic                                                         |              Jameson               |  No   |
| Kant                                                                              |            Karl Jaspers            |  No   |
| The World According to Kant                                                       |              Jauernig              |  Yes  |
| Dialecticaal Imagination                                                          |             Martin Jay             |  No   |
| Child of the Dark                                                                 |      Carolina Maria de Jesus       |  No   |
| A New German Idealism                                                             |              Johnston              |  No   |
| Man and His Symbols                                                               |                Jung                |  No   |
| Modern Man in Search of a Soul                                                    |                Jung                |  No   |
| The Software Revelation                                                           |                Kahn                |  No   |
| Opus Postumum                                                                     |                Kant                |  Yes  |
| Lectures on Logic                                                                 |                Kant                |  No   |
| Theoretical Philosophy after 1781                                                 |                Kant                |  No   |
| Critique of Pure Reason                                                           |                Kant                |  Yes  |
| Religion and Rational Theology                                                    |                Kant                | Some  |
| Anthropology, History, and Education                                              |                Kant                | Some  |
| Practical Philosophy                                                              |                Kant                | Some  |
| Critique of the Power of Judgement                                                |                Kant                |  Yes  |
| The Metaphysics of Morals                                                         |                Kant                |  Yes  |
| Religion Within the Boundaries of Mere Reason                                     |                Kant                |  Yes  |
| Groundwork of the Metaphysics of Morals                                           |                Kant                |  Yes  |
| Marx: Towards the Centre of Possibility                                           |              Karatani              |  No   |
| Transcritique                                                                     |              Karatani              |  No   |
| Critique of Religion and Philosophy                                               |              Kaufmann              |  No   |
| From Shakespeare to Existentialism                                                |              Kaufmann              |  No   |
| Hegel                                                                             |              Kaufmann              |  No   |
| The Actual and the Rational                                                       |              Kervegan              |  Yes  |
| Ross Hall                                                                         |            Andrew Perry            |  No   |
| Thinking and Being                                                                |               Kimhi                |  No   |
| Kant and Cosmopolitanism                                                          |             Kleingeld              |  Yes  |
| Creating the Kingdom of Ends                                                      |             Korsgaard              |  Yes  |
| The Sources of Normativity                                                        |             Korsgaard              |  Yes  |
| Self-Constitution                                                                 |             Korsgaard              |  Yes  |
| The Constitution of Agency                                                        |             Korsgaard              |  Yes  |
| Fellow Creatures                                                                  |             Korsgaard              |  Yes  |
| Philosophy and Revolution: From Kant to Marx                                      |             Kouvelakis             |  No   |
| Socrates Meets Kant                                                               |               Kreeft               |  No   |
| Tales of Love                                                                     |              Kristeva              |  No   |
| Ecrits                                                                            |               Lacan                |  No   |
| Book I                                                                            |               Lacan                |  No   |
| Book VII                                                                          |               Lacan                |  No   |
| Anxiety                                                                           |               Lacan                |  No   |
| The Culture of Narcissism                                                         |               Lasch                |  No   |
| Kant's Transcendental Deduction                                                   |              Laywine               |  Yes  |
| Aristotle: The Desire to Understand                                               |                Lear                |  No   |
| Critique of Everyday Life                                                         |              Lefebvre              |  No   |
| The Accumulation of Capital                                                       |             Luxemburg              |  No   |
| After Virtue                                                                      |             MacIntyre              |  No   |
| Marxism and Christianity                                                          |             MacIntyre              |  No   |
| The Prince                                                                        |            Machiavelli             |  No   |
| In the Freud Archives                                                             |               Malcom               |  Yes  |
| Psychoanalysis: The Impossible Profession                                         |               Malcom               |  No   |
| Fossil Capital                                                                    |                Malm                |  No   |
| One-Dimensional Man                                                               |              Marcuse               |  No   |
| Counterrevolution and Revolt                                                      |              Marcuse               |  No   |
| Eros and Civilization                                                             |              Marcuse               |  No   |
| Reason and Revolution                                                             |              Marcuse               |  No   |
| Selected Reading from the Works of Mao Tsetung                                    |                Mao                 |  No   |
| On Practice and Contradiction                                                     |                Mao                 |  No   |
| The Little Red Book                                                               |                Mao                 |  No   |
| The Communist Manifesto                                                           |                Marx                |  No   |
| Dialectics of Nature                                                              |               Engels               |  No   |
| The German Ideology                                                               |                Marx                | Some  |
| The Poverty of Philosophy                                                         |                Marx                |  No   |
| The Holy Family                                                                   |                Marx                |  No   |
| Result of the Direct Production Process                                           |                Marx                |  No   |
| The Holy Family                                                                   |                Marx                |  No   |
| The Marx-Engels Reader                                                            |                Marx                |  No   |
| The Political Writings                                                            |                Marx                |  Yes  |
| Capital Vol. 1                                                                    |                Marx                |  No   |
| Capital Vol. 1                                                                    |                Marx                |  No   |
| Capital Vol. 2                                                                    |                Marx                |  No   |
| Capital Vol. 3                                                                    |                Marx                |  No   |
| Early Writings                                                                    |                Marx                |  Yes  |
| Grundrisse                                                                        |                Marx                |  Yes  |
| The Origins of the Family, Private Propery and the State                          |               Engels               |  Yes  |
| Having the World in View                                                          |              McDowell              |  No   |
| Mind and World                                                                    |              McDowell              |  No   |
| Emancipation After Hegel                                                          |              McGowan               |  No   |
| Hegel's Logic and Metaphysics                                                     |              McNulty               |  Yes  |
| The Works of Mencius                                                              |             tr. Legge              | Some  |
| Beyond Leviathan                                                                  |              Meszaros              |  No   |
| October                                                                           |              Meiville              |  No   |
| Psychoanalysis and Feminism                                                       |              Mitchell              |  No   |
| The Democratic Paradox                                                            |               Mouffe               |  No   |
| Hegel and Spinoza                                                                 |               Moder                |  No   |
| Politics, Religion, and Art                                                       |              Moggach               |  No   |
| The Complete Essays                                                               |             Montaigne              | Some  |
| Bipolar Disorder                                                                  |             Mondimore              |  Yes  |
| Hegel, Haiti, and Universal History                                               |             Buck-Morss             |  No   |
| Hegel's Value                                                                     |               Moyar                |  Yes  |
| Spinal Catastrophism                                                              |              Moynihan              |  No   |
| The Experience of Freedom                                                         |               Nancy                |  No   |
| Hegel: The Restlessness of the Negative                                           |               Nancy                |  No   |
| Rosseau's Theodicy of Self-Love                                                   |             Neuhouser              |  No   |
| Rousseau's Critique of Inequality                                                 |             Neuhouser              |  No   |
| Foundations of Hegel's Social Theory                                              |             Neuhouser              |  No   |
| Hegel's Concept of Life                                                           |                 Ng                 |  Yes  |
| Thus Spake Zarathustra                                                            |             Nietzsche              |  Yes  |
| Twilight of the Idols and the Anti-Christ                                         |             Nietzsche              |  Yes  |
| Basic Writings                                                                    |             Nietzsche              |  Yes  |
| Taking Back Philosophy                                                            |             van Norden             |  No   |
| The Cosmopolitan Tradition                                                        |              Nussbaum              |  No   |
| Approaching Hegel's Logic, Obliquely                                              |               Nuzzo                |  No   |
| Rationalism in Politics                                                           |             Oakeshott              |  No   |
| Hegel's Dialectic of Desire and Recognition                                       |              O'Neill               |  No   |
| Constructing Authorities                                                          |              O'Neill               |  Yes  |
| Constructions of Reason                                                           |              O'Neill               |  Yes  |
| Pensees                                                                           |               Pascal               |  No   |
| The Categorical Imperative                                                        |               Patton               |  No   |
| German Philosophy 1760-1860                                                       |              Pinkard               |  No   |
| Hegel's Phenomenology                                                             |              Pinkard               |  Yes  |
| Hegel's Idealism                                                                  |               Pippin               |  Yes  |
| Hegel on Self-Consciousness                                                       |               Pippin               |  No   |
| Hegel's Practical Philosophy                                                      |               Pippin               |  Yes  |
| After the Beautiful                                                               |               Pippin               |  Yes  |
| Philosophy by Other Means                                                         |               Pippin               |  Yes  |
| Idealism as Modernism                                                             |               Pippin               |  No   |
| Hegel's Realm of Shadows                                                          |               Pippin               |  Yes  |
| Nietzsche, Psychology, & First Philosophy                                         |               Pippin               |  Yes  |
| The Persistence of Subjectivity                                                   |               Pippin               |  No   |
| The Culmination                                                                   |               Pippin               |  Yes  |
| Plato: Complete Works                                                             |             ed. Cooper             | Some  |
| An Essay on Man                                                                   |                Pope                |  No   |
| Humanism and Terror                                                               |           Merleau-Ponty            |  No   |
| The Merleau-Ponty Aesthetics Reader                                               |            ed. Johnson             |  No   |
| The Primacy of Perception                                                         |           Merleau-Ponty            |  No   |
| The Phenomenology of Perception                                                   |           Merleau-Ponty            |  No   |
| The Virtue of Defiance and Psychiatry                                             |               Potter               |  No   |
| Beyond the Limits of Thought                                                      |               Priest               | Some  |
| Moody Minds Distempered                                                           |               Radden               |  No   |
| Lectures on the History of Moral Philosophy                                       |               Rawls                |  No   |
| Justice as Fairness                                                               |               Rawls                |  Yes  |
| The Just                                                                          |              Ricouer               |  No   |
| The Conflict of Interpretations                                                   |              Ricouer               |  No   |
| Husserl: An Analysis of His Phenomenology                                         |              Ricouer               |  No   |
| Kant and the Law of War                                                           |              Ripstein              |  Yes  |
| Force and Freedom                                                                 |              Ripstein              |  Yes  |
| The Public Uses of Coercion                                                       |       Herlini-Karnell, Rossi       | Some  |
| Marx's Inferno                                                                    |              Roberts               |  No   |
| Virtue and Terror                                                                 |            Robespierre             |  Yes  |
| The Reactionary Mind                                                              |               Robin                |  No   |
| Black Marxism                                                                     |              Robinson              | Some  |
| The Maxims                                                                        |          La Rochefoucauld          | Some  |
| Self-Consciousness                                                                |                Rodl                |  Yes  |
| Self-Consciousness and Objectivity                                                |                Rodl                |  No   |
| The Russian Revolution                                                            |               Rodney               |  No   |
| How Europe Underdeveloped Africa                                                  |               Rodney               |  No   |
| Contingency, Irony, and Solidarity                                                |               Rorty                |  No   |
| Hegel Contra Sociology                                                            |                Rose                |  No   |
| The Shadow of God                                                                 |               Rosen                |  Yes  |
| Julie                                                                             |              Rousseau              |  No   |
| Rousseau, Judge of Jean-Jacques                                                   |              Rousseau              | Some  |
| Discourses on the Sciencces and Arts, First Discourses and Polemics               |              Rousseau              | Some  |
| The Basic Political Writings                                                      |              Rousseau              |  Yes  |
| Reveries of the Solitary Walker                                                   |              Rousseau              |  Yes  |
| On the Origins of Language                                                        |              Rousseau              |  Yes  |
| The Confessions                                                                   |              Rousseau              |  Yes  |
| Emile                                                                             |              Rousseau              |  Yes  |
| For Badiou                                                                        |                Ruda                |  No   |
| The 120 Days of Sodom and Other Writings                                          |              de Sade               |  No   |
| Orientalism                                                                       |                Said                |  Yes  |
| Culture and Imperialism                                                           |                Said                |  No   |
| A Study of Dialectic in Plato's *Parmenides*                                      |               Sanday               |  No   |
| The Concept of the Political                                                      |              Schmitt               |  Yes  |
| Theory of the Partisan                                                            |              Schmitt               |  Yes  |
| The Selected Works                                                                |                Said                |  No   |
| Critique of Dialectical Reason                                                    |               Sartre               |  No   |
| Nausea                                                                            |               Sartre               |  Yes  |
| The Sappho Companion                                                              |              Reynolds              |  No   |
| On University Studies                                                             |             Schelling              |  No   |
| On the Aesthetic Education of Man                                                 |              Schiller              |  No   |
| Political Theology                                                                |              Schmitt               |  No   |
| Dictatorship                                                                      |              Schmitt               |  No   |
| The Invention of Autonomy                                                         |             Schneewind             |  Yes  |
| Essays and Aphorism                                                               |            Schopenhauer            |  No   |
| Apperception and Self-Consciousness in German Idealism                            |             Schulting              |  Yes  |
| The Bounds of Transcendental Logic                                                |             Schulting              |  No   |
| Kant's Radical Subjectivism                                                       |             Schulting              |  No   |
| Kant's Transcendental Deduction from Apperception                                 |             Schulting              |  No   |
| Hegel's Critique of Kant                                                          |             Sedgewick              |  No   |
| Selected Letters                                                                  |               Seneca               |  No   |
| Dark Ghettos                                                                      |               Shelby               |  No   |
| Men & Citizens                                                                    |               Shklar               |  No   |
| A Commentary on Kant's Critique of Pure Reason                                    |               Smith                |  No   |
| The Philosophy of David Hume                                                      |               Smith                |  No   |
| Irrationality                                                                     |               Smith                |  No   |
| The Collected Works                                                               |              Spinoza               |  No   |
| In the Spirit of Hegel                                                            |              Solomon               |  No   |
| Intellectual and Manual Labor                                                     |            Sohn-Rethel             | Some  |
| How Propaganda Works                                                              |              Stanley               |  No   |
| How Fascism Works                                                                 |              Stanley               |  Yes  |
| Hegel's Century                                                                   |              Stewart               |  No   |
| On Hegel                                                                          |              Strauss               |  No   |
| Defense of the Philosophical Life                                                 |              Strauss               |  No   |
| The Political Philosophy of Hobbes                                                |              Strauss               |  No   |
| What is Political Philosophy?                                                     |              Strauss               |  No   |
| Individuals                                                                       |           P.F. Strawson            |  No   |
| The Significance of Philosophical Skepticism                                      |               Stroud               |  No   |
| Hume                                                                              |               Stroud               |  No   |
| Hegel                                                                             |               Taylor               |  Yes  |
| A Secular Age                                                                     |               Taylor               |  No   |
| Reconsidering Reparations                                                         |               Taiwo                | Some  |
| Elite Capture                                                                     |               Taiwo                |  Yes  |
| A People's Guide to Capitalism                                                    |               Thier                |  No   |
| Mind in Life                                                                      |              Thompson              |  No   |
| Life and Action                                                                   |              Thompson              |  No   |
| Logic in the Husserlian Context                                                   |                Tito                |  No   |
| Literature and Revolution                                                         |              Trotsky               |  No   |
| Terrorism and Communism                                                           |              Trotsky               |  No   |
| The Desire of Psychoanalysis                                                      |             Tupinamba              |  No   |
| The Art of War                                                                    |              Sun Tzu               |  No   |
| Sex, Love and Gender                                                              |               Varden               |  Yes  |
| Freedom and the End of Reason                                                     |              Velkley               |  No   |
| Political Writings                                                                |              Voltaire              |  No   |
| The Cornel West Reader                                                            |            Cornel West             | Some  |
| History & Truth in Hegel's *Phenomenology*                                        |              Westphal              |  No   |
| The Conservative Sensibility                                                      |                Will                |  No   |
| The Sources of Metaphysics                                                        |             Willaschek             |  Yes  |
| Ethics and the Limits of Philosophy                                               |              Williams              | Some  |
| Hegel and the Future of Systematic Philosophy                                     |              Winfield              |  No   |
| The Blue and Brown Books                                                          |            Wittgenstein            |  No   |
| Tractatus Logico-Philosophicus                                                    |            Wittgenstein            |  No   |
| Tractatus Logico-Philosophicus                                                    |            Wittgenstein            |  No   |
| Notebooks 1914-1918                                                               |            Wittgenstein            |  No   |
| Remarks on Psychology                                                             |            Wittgenstein            |  No   |
| Carceral Capitalism                                                               |                Wang                |  No   |
| Charisma and Disenchantment                                                       |               Weber                |  No   |
| The Protestant Work Ethics and the "Spirit" of Capitalism                         |               Weber                |  No   |
| Rousseau, the age of Enlightenment, and Their Legacies                            |               Wokler               |  No   |
| Kant and Religion                                                                 |                Wood                |  Yes  |
| Kantian Ethics                                                                    |                Wood                |  No   |
| Hegel's Ethical Thought                                                           |                Wood                |  Yes  |
| Malcom X                                                                          |             Alex Haley             | Some  |
| The Portable Malcom X Reader                                                      |              Penguin               |  No   |
| Conversations of Socrates                                                         |              Xenophon              |  No   |
| Spinoza and Other Heretics                                                        |               Yovel                |  No   |
| Hegel's Preface to the *Phenomenology of Spirit*                                  |               Yovel                |  No   |
| Free                                                                              |                Ypi                 |  No   |
| Less than Nothing                                                                 |               Zizek                |  Yes  |
| Did Somebody Say Totalitarianism?                                                 |               Zizek                |  No   |
| Revolution at the Gates                                                           |               Zizek                |  No   |
| The Sublime Object of Ideology                                                    |               Zizek                |  Yes  |
| Looking Awry                                                                      |               Zizek                |  No   |
| What is Sex?                                                                      |              Zupancic              |  No   |
| The Cynic Philosophers from Diogenes to Julian                                    |              Penguin               | Some  |
| Readings in Classical Chinese Philosophy                                          |      ed. Ivanhoe, Van Norden       |  Yes  |
| Nietzsche in Itality                                                              |             Pourtales              |  No   |
| Hegel: The Philosopher of Freedom                                                 |               Vieweg               |  No   |
| Kant                                                                              |                Kuhn                |  No   |
| Kant and the Early Moderns                                                        |      ed. Gardner, Longuenesse      |  No   |
| Hegel on Action                                                                   |        ed. Laitenen, Sandis        | Some  |
| Hegel on Philosophy in History                                                    |        ed. Zuckert, Kreines        | Some  |
| New Anti-Kant                                                                     |        ed. Lapointe, Tolley        |  No   |
| Hegel                                                                             |           ed. MacIntyre            |  Yes  |
| The Philosophical Rupture Between Fichte and Schelling                            |          ed. Vater, Wood           |  No   |
| Between Kant and Hegel                                                            |      ed. di Giovanni, Harris       |  No   |
| The Cambridge Companion to Plato's Republic                                       |             Cambridge              |  No   |
| Early Modern German Philosophy 1690-1750                                          |              ed. Dyck              |  No   |
| The Young Hegelians                                                               |          ed. Stepelevich           |  Yes  |
| The British Moralists                                                             |             ed. Bigge              |  No   |
| Rethinking German Idealism                                                        |           McGrath, Carew           |  No   |
| The First Philosophers: The Presocratics and the Sophists                         |               Oxford               |  Yes  |
| The Kant Lexicon                                                                  |             Cambridge              |  N/A  |
| The Cambridge Companion to Kant and Modern Philosophy                             |             Cambridge              |  Yes  |
| The Cambridge Companion to Kant                                                   |             Cambridge              |  Yes  |
| Kant's Transcendental Deduction and the Theory of Apperception                    |             de Gruyter             | Some  |
| Contemporary Kantian Metaphysics                                                  |      ed. Baiasu, Bird, Moore       |  No   |
| Practical Philosophy from Kant to Hegel                                           |        ed. Clarke, Gottlieb        |  Yes  |
| The Sensible and Intelligible Worlds                                              |         ed. Schafer, Stang         |  No   |
| Normativity and Agency                                                            | ed. Schapiro, Ebels-Duggan, Street |  No   |
| The Transcendental Turn                                                           |              Gardner               | Some  |
| Immanuel Kant's *The Groundwork of the Metaphysics of Morals*                     |          Schonecker, Wood          |  Yes  |
| The Bible                                                                         |            NSRV, Oxford            |  No   |
| The Cambridge Companion to Hegel                                                  |             Cambridge              |  No   |
| A Hegel Dictionary                                                                |               Inwood               |  N/A  |
| Les Philosophes                                                                   |             ed. Torrey             |  No   |

### Home desk
| Title                                                                             |          Author(s)           | Read  |
| :-------------------------------------------------------------------------------- | :--------------------------: | :---: |
| On Writing Well                                                                   |           Zinsser            |  Yes  |
| The Elements of Style                                                             |        Strunk, White         |  Yes  |
| Emily Dickonson's Poems                                                           |           Harvard            |  No   |
| The Poems of T.S. Eliot                                                           |             FSG              |  No   |
| A Farewell to Arms & Other Writings 1927-1932                                     |       Ernest Hemingway       |  No   |
| Poetry & Prose                                                                    |           Whitman            | Some  |
| Rights of Man, Common Sense, and Other Political Writings                         |         Thomas Paine         |  No   |
| The Varieties of Religious Experiences                                            |        William James         |  No   |
| Writings                                                                          |      Benjamin Franklin       |  No   |
| The Man Who Understood Democracy                                                  |             Zunz             |  No   |
| American Philosophy                                                               |             Kaag             |  Yes  |
| The Pragmatism Reader                                                             |       ed. Louis Menand       |  No   |
| The Rise of American Philosophy                                                   |           Kuklick            |  No   |
| A History of Philosophy in America                                                |           Kuklick            |  Yes  |
| Pierce and the Thread of Nominalism                                               |           Forster            |  No   |
| The Good Die Young                                                                |           Jacobin            |  No   |
| The Liberal Imagination                                                           |           Trilling           |  No   |
| The White Album                                                                   |            Didion            |  No   |
| Play it as it Lays                                                                |            Didion            |  No   |
| The Selected Essays of Gore Vidal                                                 |            Vidal             |  No   |
| The Mind of an Outlaw                                                             |            Mailer            |  No   |
| When the Clock Broke                                                              |             Ganz             |  Yes  |
| The Back Channel                                                                  |            Burns             |  No   |
| War                                                                               |           Woodward           |  Yes  |
| The Glorious American Essay                                                       |          ed. Lopate          | Some  |
| The Best American Essays of the Century                                           |          ed. Oates           | Some  |
| Metamorphoses                                                                     |             Ovid             |  No   |
| The Poems of Exile                                                                |             Ovid             |  No   |
| The Art of Love                                                                   |             Ovid             |  No   |
| Myths from Mesopotamia                                                            |            Oxford            |  No   |
| Theogony and Works and Days                                                       |            Hesiod            |  No   |
| Herodotus                                                                         |        The Histories         |  No   |
| Euripides                                                                         |    Medea and Other Plays     |  No   |
| Euripedes II                                                                      |           Chicago            |  No   |
| Euripedes V                                                                       |           Chicago            |  No   |
| The Iliad                                                                         |            Homer             |  No   |
| The Odyssey                                                                       |            Homer             |  No   |
| The Aeneid                                                                        |            Virgil            |  No   |
| Lysistrata and other Plays                                                        |         Aristophanes         |  No   |
| Sophocles                                                                         |    The Three Theban Plays    | Some  |
| The Bhagavad Gita                                                                 |           Penguin            |  No   |
| The Homeric Hymns                                                                 |        Johns Hopkins         |  No   |
| The Norton Anthology of Western Philosophy After Kant: The Interpretive Tradition |         ed. Schacht          | Some  |
| The Norton Anthology of Western Philosophy After Kant: The Analytic Tradition     |     ed. Conant, Elliott      | Some  |
| Classics of American Political and Constitutional Thought                         |           Hackett            | Some  |
| African American Political Thought                                                |      ed. Rogers, Turner      | Some  |
| The American Revolution                                                           |      ed. Gordon S. Wood      |  No   |
| The Debate on the Constitution                                                    |      ed. Bernard Bailyn      |  No   |
| The Speeches & Writings of Abraham Lincoln                                        |   ed. Don E. Fehrenbacher    |  No   |
| The Civil War: Told By Those Who Lived It                                         |  ed. Brooks D. Simpson etc   |  No   |
| American Poetry: The Nineteenth Century                                           |      ed. John Hollander      |  No   |
| H. L. Mencken: Prejudices                                                         | ed. Marion Elizabeth Rodgers |  No   |
| The Daemon Knows                                                                  |         Harold Bloom         |  Yes  |
| Classics of American Literature                                                   |        D.H. Lawrence         |  No   |

### Work desk
| Title                                                             |      Author(s)       | Read  |
| :---------------------------------------------------------------- | :------------------: | :---: |
| The New Makers of Modern Strategy                                 |      ed. Brands      | Some  |
| Scaling People                                                    |       Johnson        |  Yes  |
| The Systems Bible                                                 |         Gall         |  Yes  |
| Wherever You Go, There You Are                                    |      Kabat-Zinn      |  Yes  |
| Basho                                                             |     Fitzsimmons      |  No   |
| Designing Data-Intensive Applications                             |      Kleppmann       |  No   |
| Clean Architecture                                                |        Martin        |  Yes  |
| Monolith to Microservices                                         |       Khononov       |  Yes  |
| Learning Domain-Driven Design                                     |        Newman        |  Yes  |
| Programming Ruby                                                  | Hunt, Fowler, Thomas |  No   |
| Ideas That Created the Future: Classic Papers of Computer Science |      ed. Lewis       | Some  |
| The Pragmatic Programmer                                          |     Hunt, Thomas     |  Yes  |
| Leadership Without Easy Answers                                   |       Heifetz        |  Yes  |
| The Practice of Adaptive Leadership                               |    Heifetz et al.    |  Yes  |
| Say It Well                                                       |    Terry Szuplat     |  Yes  |

