+++
title = "The View from Nowhere"

description = "The first philosopher with a large body of primary and secondary texts that I have dedicated myself to studying is Kant. In the first _Critique_ especially, but all throughout, there are still quite raging debates in over what Kant meant. One major narrative, so I am told, of the Anglo world is that Henry Allison looms quite large right now, but before him was Peter Strawson for bringing Kant to their academic circles, Allison doing a more systematic, holistic reading than Strawson who did not think all of Kant would be palatable to the Anglo-analytics."
+++
The first philosopher with a large body of primary and secondary texts that I have dedicated myself to studying is Kant. In the first _Critique_ especially, but all throughout, there are still quite raging debates in over what Kant meant. One major narrative, so I am told, of the Anglo world is that Henry Allison looms quite large right now, but before him was Peter Strawson for bringing Kant to their academic circles, Allison doing a more systematic, holistic reading than Strawson who did not think all of Kant would be palatable to the Anglo-analytics.

After the first _Critique,_ Allison’s revised _Kant’s Transcendental Idealism_ was the first secondary literature I read on it. One of the main sort of “opponents” or “interlocutors” the book was Strawson, who I have not and have not yet read. The way Allison talks of Strawson makes it seem like they have read almost entirely different books. For example, Strawson, Allison thinks, is so far off the mark on understanding Kant that he himself is imposing his own “transcendental realist” (_Kant’s Transcendental Idealism,_ p. 255) commitments/baggage/presuppositions onto Kant, which if true, poses quite a problem for someone if they are supposedly articulating Kant’s transcendental idealism.

This does not leave. You have further debates where some Kant interpreters are accused of articulating Kant-as-Leibniz, Kant-as-Berkeley, etc. A recent book I read was Anja Jauernig’s book _The World According to Kant_. Here, she defends a two-world reading, as opposed to Allison and others who are considered one-world or two-aspect (possibly not the same teams). Allison does not think that there exists a thing-in-itself, or rather deeper, has any ontological reality. Jauernig argues otherwise and says that without two distinct (numerically as well) ontological worlds, we cannot make sense of Kant’s philosophy, his transcendental idealism or empirical realism, his conception of the will and freedom as well.

The interpretations seem to start to vary so radically that it raises the question “Is Kant even consistent himself in his own philosophy?”. Some analytic types who are against this sort of ritualistic religious practice us more historically-inclined, embedded in tradition philosophers might find this whole practice absurd. Two-world or two-aspect, they might say, just go ahead and articulate the theory itself independent of Kant. Are there not essential characteristics that you can advocate and, more importantly, adjudicate independently of the text? Allison and Jauernig, instead of spending 600 pages and decades trying to match interpretation to text, as if your interpretation being too far from the literal text itself will find you in the front of the North American Kant Society council and sentenced to life in a cell under Andrew Chignell’s office. At least, I imagine this is how some of the analytics view our worship of texts like the _Critique of Pure Reason_.

Not going there with this post, I would rather go in a different direction. For a short blurb, I do not think that the analytics avoid the problem of historicity and tradition, they have their own sacred texts, be it a paper by Quine or Lewis, and they have interpretive debates as well, maybe we are historical folks are just different in degree, not kind! Where I do want to go is instead, what if we acknowledge that when we approach a text, or interpret anything for that matter, we do our best to be aware of the reasons why we are interpreting a text in such a way?

Allison for example, might himself be coming from a world where something like multiple levels of reality, as Jauernig or Strawson want to argue, is seen as ridiculous. Allison might come from a more empiricist background, and so sees Kant as articulating a minimal epistemological revolution about interpreting a more similar conception of the world scientists may have, but just getting all the stuff about truth and knowledge working well. Jauernig herself is explicit about her commitments to Kant’s influence by Leibniz. A similar thing might be at play in Hegel interpretation. Depending on your views of Hegel or Spinoza, you either see Hegel as a more radical Kantian or a post-Kantian yet Spinozist who might have more in common overall with pre-critical metaphysicians.

It is of varying degree to what extent philosophers are explicit about this. Generally, we tend to think that we have sort of _un_mediated access to a text itself, sometimes ironically enough, even if we are talking about philosophers who would find it laughable that we ever have immediate access to anything. Some philosophers may take themselves to be telling a story about the context of what some philosopher, using Kant, was writing about. Depending on the Kantian, they might talk about or emphasize most Hume, Leibniz, Wolff, Descartes, Rousseau, Newton, Crusius, Hutcheson, Locke, and more. They might even go in depth a bit into Kant’s own pietist upbringing or the German intellectual life, or Kant’s late reactions to criticisms of his critical philosophy. If you ever get something explicit about a scholar’s background, it comes in the form of prior work they have done on these philosophers, say Hume or Leibniz or Plato.

Not Brandom. Brandom says

> In a pastiche on W. G. Pogson-Smith’s question about Spinoza, “If a man chooses to bind the spirit of Christ in the fetters of Euclid, how shall he find readers?” Richard Rorty once described Wilfrid Sellars as “binding the spirit of Hegel in the fetters of Carnap.” When I finished my graduate studies, considering my principal teachers at Princeton I thought of myself as, if not quite seeking to bind the spirit of Rorty in the fetters of David Lewis, at least aiming at expressing Rorty’s spirit in something like the language of Lewis. _A Spirit of Trust_ articulates Hegel’s views in a regimented idiom specially crafted for that expressive purpose.
>
> [1](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-1-48019475)

Or again, describing his Hegel as more of a “Bregel”

> When approached by students steeped in Hegel who found it difficult to connect what I was saying with what they had read and been taught about Hegel, he recommended that they first concentrate on understanding what this philosopher “Bregel” had to say, and only when they had a solid grip on that, set to work on figuring out how his views were related to Hegel’s. (He had the good grace to assure them that in his view, both undertakings would be well worth their while.)
>
> [2](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-2-48019475)

This is not to mention everything else he says throughout his reading of Hegel in _A Spirit of Trust_. Before you even get to the text, you get a reference abbreviations page. There are four Hegel books, the _Phenomenology of Spirit_, the _Philosophy of Right_, the _Philosophy of Mind_, and the _Science of Logic_. There are then two Sellars books, two Wittgenstein books, and McDowell’s _Mind and World_, who the book is so lovely dedicated to.

There are eight citations to Brandom’s prior works, however. For a book on Hegel, you would expect, maybe, more of, well more of what I described above. For Hegel, you would expect something like Karen Ng’s _Hegel’s Concept of Life_, which features the big four of Kant, Fichte, Schelling and Hegel. Or you might see Robert Pippin’s _Realm of Shadows_, where we have Kant, Aristotle, and Hegel. Going deeper, you might have some more on Descartes, Spinoza, Hume, or the German Romantics. Not even just this, Brandom rarely engages with any Hegel scholars of past or present in a book with a subtitle “A reading of Hegel’s _Phenomenology_”.

Brandom says no. Not only this, he broke another unspoken rule of our “religious practice”. Mark Alznauer (thank you to my buddy Sam for showing me this review) puts it in the best way possible

> In his _Autobiography_, Mark Twain talks about what it was like to improvise stories to his daughters in their home in Hartford. You have to think that Twain was well-suited for this particular parental duty, yet he complains about a burdensome constraint that was imposed on his story-telling by his children: every tale had to incorporate each of the pictures and knick-knacks that were on the mantel in the library, and always in the exact same order, beginning with the portrait of the cat on the left and ending with the watercolor of the girl on the far right. Anyone offering a new interpretation of the _Phenomenology of Spirit_ faces a similarly challenging task. Given the longstanding interpretive debates surrounding the book, there is always room for a new reading, but any such reading faces a major constraint: it has to touch on every major chapter and in the right order. It must show how consciousness (whatever that is) leads to self-consciousness (whatever that may be), and so forth, until humans reach their predetermined destination, the arrival of spirit's self-knowledge.
>
> [3](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-3-48019475)

The important point for Hegel interpretation Alznauer touches on is that, Hegel quite famously has some mysterious philosophy that is dialectical and in the _Preface_ to the _Phenomenology of Spirit_ ties this dialectical method to the literal presentation of the book’s own writing style and progression. In the _Phenomenology of Mind_, we start with Sense-Certainty and thus move onto Perception. In the _Philosophy of Right_, it means we start with Abstract Right and move onto Morality. In the _Science of Logic_, it means we start with _pure Being_, how it “vanishes” into Nothingness, but in that we somehow end up with Becoming, and the book continues. At the end of the _Logic_, we learn that now we can move onto the _Philosophy of Nature_.

Like the Twain example, it seems as if the Hegel scholar is thus doomed to only do Hegel interpretation by retracing the above mentioned books like the knick-knacks and other objects that happened to be on his mantle for his children (in this metaphor, we are the children of Pippin, Ng, Brandom, etc it seems). The task of the Hegel scholar is now Sisyphusean, we roll our boulder from Sense-Certainty to Perception, maybe we do it a bit better than Hegel or with more ease as we pour over the text and transitions more than he himself did, the thousands of us united in pushing the boulder, but we still are just pushing it up the hill. Furthermore, it seems as if we are continually doomed for it to roll back down when faced with possibly devastating or perplexing issues at least with the “letter” of Hegel’s philosophy, even worse if the problems are with the “spirit” of Hegel’s philosophy.

[![Видео Robert Brandom](https://substackcdn.com/image/fetch/w_1456,c_limit,f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F745c6e1f-1be3-4ad9-bbbb-7211c3986870_480x360.jpeg)](https://substackcdn.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F745c6e1f-1be3-4ad9-bbbb-7211c3986870_480x360.jpeg)

Brandom says “fuck that” to all the Hegel scholars and zooms past them pushing their boulder up the hill. From the perspective of the Hegel scholars, he seems to be missing the point. We have no other task than to push the boulder. From Brandom’s perspective, I can only guess, he sees the task as absurd not in an edgy French way, but in the pejorative way, at least, he himself does not find it to be fitting while he may not be as negative about it and respect different approaches to scholarship (more likely, he seems very sweet and kind from his lecture videos and the grad students on twitter who sing his praise, which I too share because I like the Hegel scholars a lot too).

Whether or not we should adjudicate if Brandom is right in his approach, or even if on the terms of his own hermeneutical approach (imminent critique style) does he succeed, I would like to propose something similar Hegelians discuss in Hegel’s philosophy. What if there is no “view from nowhere”, we are always viewing a text or “reality” from somewhere? If so, then Brandom and the Hegelians all share such an hermeneutical difficulty, where some of us are more explicit about our philosophical baggage in interpreting a text than others. In Hegel, we talk of the moral valet or the alienated subject who thinks they have some sort of immediate hold on truth. In Bregelese maybe, they think their normative attitudes institute normative statuses by themselves, without any mediation through another self-conscious subject, some form of hyper-independence.

In a similar way, the way we approach and read a text is mediated through, as the analytic philosopher we mentioned above might say, our own philosophical baggage (positions). Maybe it is better to do the reverse and articulate a two-world or two-aspect view independently of Kant, and maybe this partly explains interpretive debates and why they do not really end.

In another Kant digression, there are multiple different types of readings of Kant’s _Doctrine of Right_. Helga Varden in her “Immanuel Kant — Justice as Freedom” paper (her book too), shows that three major ways of interpreting Kant’s political philosophy is either something like a successor to Hobbes, a successor to Locke, or a success to some Republican-Rousseauean political philosophy (she and her former advisor Arthur Ripstein argue, correctly, for the last option). It should not be surprising that Hobbes, Locke, and Rousseau are thus some of the most taught political philosophers. It seems likely that if, prior to a study of the _Doctrine of Right_, you were convinced by one of the three, the parts of Kant that seem more Hobbessian/Lockean/Rousseauean might stick out more and you center your interpretation around that one instead.

This might also explain certain Hegelian debates. Robert Pippin has worked extensively on Kant’s philosophy, and so it is no wonder maybe that, thinking independently of Hegel, there is a strong influence that he thinks in “general” Kant is right about much. There is quite a bit of critique of Kant in Hegel, and Pippin might best explain this as more narcissism of small differences at times (which there definitely is) and when disagreeing with Kant, Hegel is instead radicalizing the Kantian philosophy, but continuing the spirit of it. If you are Houlgate, you might have a stronger leaning towards Spinoza, pre-Kantian metaphysics, and you have your own particular reading of Kant (something Dennis Schulting seems weary of he has told me on twitter). It might be no wonder then, that Houlgate has written a book on the opening of the _Logic_ and the _Doctrine of Being_ (not that he has written elsewhere on the rest of Hegel, he does know Hegel well) without spending too much time, relative to the parts where Hegel is more in a Spinozist metaphysician discussion of philosophy than Pippin (Ng as well) who stress more of the aspect of judgement and apperception that appear in the last part, the subjective logic. This is, I think, a striking similarity to the interpretive debates of whether Kant’s political philosophy is more in line with Hobbes, Locke, or Rousseau.

Back to Brandom, Brandom I do not think has this problem because Brandom is quite more explicit that “his Hegel”, a term people use to describe their interpretation all the time (in the new Houlgate, he frequently will use the phrase “my Hegel” or for Pippin say “his Hegel”, so implicit already in our language of scholarship is this idea of “my Hegel”). Brandom is aware, as mentioned above, that we must first think of it as Bregel (Houlgate → Hougle, Pippin → Pigle, Ng → Ngle, … hehe).

The reason I find Brandom’s cavalier attitude towards “reading” the _Phenomenology_ so interesting if not great is that he is explicit about his baggage. Brandom tells a similar German idealist, post-Kantian, Rortian story about representation from Descartes to Kant. There is a long list of more analytic philosophers mentioned, going through the index I see some more well known names of Anscombe, Berlin, Chomsky, Davidson, Dennet, Frege, Freud, Foucault, Kripke, Lewis, Marx, Nietszche, Quine, Searle, Sellars, Strawson, Tarski, and Wittgenstein. You get a very little bit of McDowell and Pippin. You get a decent amount of pre-Hegelians such as Aristotle, Descartes, Newton, Berkeley, Spinoza, Kant.

If I had to say in general where Brandom is coming from, it was the pragmatist tradition, the analytic philosophy tradition, which also seems hard to differentiate at times from the recent philosophy of language tradition as well. Due, most likely from Kant’s influence on both Hegel and those three traditions, it is hard to avoid doing a lot of Kant as well.

But throughout the book, Brandom is very explicit not just on problems that are in Hegel scholarship, but problems he finds Hegel as a solution or path forward for his own prior life. How do we know, in Hegel, when someone is truly committed to something they say they are? Brandom reads Hegel in with the pragmatists. Hegel’s inverted world is put into conversation with Lewis’s possible world semantics. Sense and reference are mapped onto phenomena and noumena. Modal logic is seen in the Kantian separation of the normativity of objective reality and the subject, alethic and deontic modality, that he thinks Hegel takes up. He reads sees the pragmatist in Hegel as taking on the idea of judgement and thought as minimal units of commitments, commitments being the important aspect here for Hegel, that unite Kant, Frege, and Wittgenstein. Hegel’s theory of action is seen as essentially Davidsonean. He sees the sort of Kantian-Hegelian idea of freedom, the negative within the positive as explaining the normativity of language and explaining the “explosion of expressivity” language provides. He sees Hegel anticipating the Sellarsian “-ing/-ed” ambiguity.

You can see the traces of Brandom’s philosophical journey to Hegel, his discovery of Hegel, his reading of Hegel into people who are, in time after Hegel, and his grappling of Hegel with some of the influences of Hegel such as Aristotle, Spinoza, Kant, Descartes, Berkeley, etc.

This sounds cool and all, but of course, the “why does this matter” lurks in the background. One is I think, to maybe at the least ease interpretive debates. Many times we slide into a debate amount “mere semantics”, which usually means we are squabbling over nothing. Sometimes though, and inevitably/necessarily that is, we slide from interpreting the text and interpreting the scholar’s interpretation. This is necessary, in Bregel, we can talk about the way things are merely for someone and the way things are in themselves. When two scholars debate over some text, the two scholar’s interpretations are the “sense”, the text the “reference”. Eventually, one scholar provides better reasons and what was for consciousness before becomes seen as merely two consciousness, we were wrong. Bregel thinks this is a fundamental feature of experience itself, so why would reading a book somehow escape this? Graham Priest would welcome this self-reflexivity as indicative of the task of philosophy. So, if we keep in mind that Brandom’s book is Bregel or Houlgate’s is Hougle, maybe we can keep track of which thing we are discussing a bit easier and be aware of why, not only of others, but more importantly like Brandom, be aware of what we are getting out of a text and reading into it, which we do consciously or unconsciously no matter what.

The last I think, of why I like Brandom’s approach, is for a reason I like Zizek’s _Less Than Nothing_, Priest’s _Beyond the Limits of Thought_, Pippin’s _Realm of Shadows_, Ng’s _Hegel’s Concept of Life_, and many other books that do their best to show a thinker as not isolated, but in conversation with others. More important, Brandom, Zizek, and Priest take this a step further. Brandom puts in conversation people in quite different traditions of philosophy with Hegel, such as the analytics and philosophers of language. Zizek puts Hegel in conversation with psychoanalysis (analytics as well), Plato, Marxism, film, French philosophy. Priest puts Hegel in conversation with mathematicians, contemporary logic, ancient Greek philosophers, and Nāgārjuna.

Please allow me to quote a few “bangers” as the youth say (I’m the youth) from Walter Benjamin’s _The Task of the Translator_ which I will comment on and conclude this post with.

> (1) Languages are not strangers to one another, but are, _a priori_ and apart from all historical relationships, interrelated in what they want to express.
>
> (2) In translation the original rises to a higher and purer linguistic air, as it were. It cannot live there permanently, to be sure, and it certainly does not reach it into its entirety. Yet, in a singularly impressive manner, at least it points the way to this region: the predestined, hitherto inaccessible realm of reconciliation and fulfillment of languages. The transfer can never be total, but what reaches this region is that element in a translation which goes beyond transmittal of subject matter.
>
> (3) Rather, for the sake of pure language, a free translation bases the test on its own language. It is the task fo the translator to release in his own language that pure language which is under the spell of another, to liberate the language imprisoned in a work in his re-creation of that work. … A simile may help here. Just as a tangent touches a circle lightly and at but one point, with this touch rather than with the point setting the law according to which it is to continue on its straight path to infinity, a translation touches the original lightly and only at the infinitely small point of the sense, thereupon pursuing its own course according to the laws of fidelity in the freedom of linguistic flux.
>
> (4) The basic error of the translator is that he preserves the state in which his own language happens to be instead of allowing his language to be powerfully affected by the foreign tongue. … He must expand and deepen his language by means of the foreign language.

In (1), we see an almost explicit formulation of something like sense and reference. But in terms of philosophical traditions, say between analytic philosophy and Hegel, which might have one of the least connected “historical relationships”, Brandom thinks like Benjamin here, that _a priori_ even Frege, Wittgenstein, Tarsky, Lewis, Quine, Davidson, Kripke, you name it, can be put in conversation with each other. There is thus an element of Brandom as philosopher and Brandom as translator. However, this is a translation that goes on in all language, between the different senses, language is the manifestation of self-consciousness. As Brandom himself says

> We see language, then, in its characteristic significance as the expressive medium for conceptual normativity—the sea in which normative fish swim.
>
> [4](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-4-48019475)

If us self-conscious, normative beings swim in language, then when doing philosophy, we must also think like a translator. Even when closed in a more historically connected tradition of Hegel scholarship between say Houlgate and Pippin, Hegel and analytic philosophy, Bregel and Hegel, or putting Hegel and Nāgārjuna together, we cannot escape some sort of translation task as long due to our linguistic and normative natures.

In (2), one can easily describe Brandom’s book as “singular” and “impressive”. Brandom himself even uses linguistic language when he talks of reconciliation and forgiveness, which are essential for the corner stone of his work, and one way or another for all Hegel interpreters, Hegel’s philosophy.

> So what is forgiveness? Forgiving, like confessing, is a _speech_ act, something done in _language._ It is doing something by saying something. That is why Hegel talks about it in terms of the “word of reconciliation [Versöhnung].”
>
> [5](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-5-48019475)

Arguably as well, in Hegel, maybe not Brandom

[6](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-6-48019475)

, there is something total about Brandom’s reconciliation of traditional _Sittlichkeit_ and modern subjectivity, but that is a point for debate later.

In (3), we can see how _A Spirit of Trust_ is like a tangent, approaching Hegel. However, like above where I say Brandom zooms past the Hegel scholars playing Sisyphus, lightly touching the circle that is Hegel but continuing its own course, starting something anew.

In both (3) and (4) we see something as well about how the translation takes us to a new understanding of the original work and the languages involved. Alznauer says of Brandom

> One of the most impressive things about Robert Brandom's _A Spirit of Trust_ is that it manages to offer a reading of the Phenomenology that honors this constraint despite starting with an entirely unprecedented claim about what the whole book is about.
>
> [7](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-7-48019475)

Pippin says something similar about Zizek’s own similar Brandomean translation task in _Less Than Nothing_

> … he has written a serious attempt to re-animate or re-actualize Hegel (in the light of Lacanian meta-psychology and so in a form he wants to call “materialist”) …
>
> [8](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-8-48019475)

At once, Brandom has breathed new life into not only the Hegel tradition, less historically connected traditions of analytic philosophy. Furthermore, I do not think that one, taking Brandom seriously and keeping in mind that his Hegel, like everyone’s, is _his_ Hegel, will not have their own “language” seriously affected, in this case, the languages being the traditions of Hegel and analytic philosophy. The Hegelian deepens their language in Brandom and the analytic deepens their language in Brandom.

Brandom shares, I think, something of an aim of mine with respect to philosophy to which Priest and Zizek do too, is trying to put as much historically disconnected philosophical traditions in conversation with each other. I truly do not believe that analytic philosophy is better in any sort objective way, if anything, it is better in the exact way Hegel understands modernity to be better than traditional _Sittlichkeit_, but this does not mean that modernity is lacking negativity or contradictions, much like analytic philosophy, and much like the task of the translator.

Nothing is “transcendentally” better in any traditions writing style. I for one have an easier time reading _The Phenomenology of Spirit_ or the _Critique of Pure Reason_ than if I picked up any major analytic philosophy paper due to mainly reading Kant and Hegel for the past two years. I have picked up and put down a few after a couple pages, which, funnily enough, is exactly what analytics say about Hegel sometimes.

This principle can be extended further, this principle of translating different philosophical traditions. In the book on Martin Luther King Jr. called _To Shape a New World_, MLK scholars put MLK in conversation with some of his own personal influences such as Kant, Hegel, Gandhi, and Niebuhr, they put him in conversation with popular political theorists Rawls and Marx, they put him in conversation with black American political thinkers and thought such as Booker T. Washington, W.E.B. Du Bois, Frederick Douglass, Malcom X, the Black Power / Black Nationalism movements, and most importantly his relationship to Christianity and political movements as a whole.

Philosopher’s kid about “footnotes to Plato”, but there is a sense that Benjamin could interpret that as the idea that we are still all talking about the same thing, what Plato was talking about. Schneewind at the end of his _The Invention of Autonomy_ has a note on what type of “story” is appropriate to tell about morality, the Socrates or the Pythagoras story, when was this particular notion of the good life invented. But Schneewind and other philosophers, more recently Graeber and Wengrow in _The Dawn of Humanity_ all argue that we have had ideas about freedom, math, science, etc., for quite some time throughout all of history, even in literally historically separate traditions and societies.

If, in a Hegelian way, give up our one-sided subjectivity, our dogmatic adherence to a particular writing or philosophical tradition and see it as not transcendental but reified, and engage in a bit more of what Benjamin calls reconciliation in our translations, maybe we will deeper our own understanding of our writing and philosophical traditions we adhere to now, finding freedom in this linguistic flux. Our work, much like Brandom’s, rises to some higher air, but it will not live there. But it does point us to a beyond region, where analytics might discover more in Hegel, and Hegelians in analytic philosophy.

Something like this, I think, is exactly what Hegel meant by “Absolute Knowing”.

[1](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-anchor-1-48019475)

Brandom, R. B. (2021). Replies to Honneth, McDowell, Pippin, and Stern. _Philosophy and Phenomenological Research_, 103, 756.

[2](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-anchor-2-48019475)

_ibid_, p. 757.

[3](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-anchor-3-48019475)

Mark V. Alznauer, "Hegel, Brandom, and Semantic Descent: Comments on _A Spirit of Trust_ by Robert B. Brandom (Harvard, 2019)," _Existenz_ 15/1 (2020), 42.

[4](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-anchor-4-48019475)

Robert Brandom, _A Spirit of Trust_, p. 510

[5](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-anchor-5-48019475)

_ibid,_ p. 598

[6](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-anchor-6-48019475)

Zizek is a fan of Benjamin’s theory of history, and the view articulated in this article of Zizek critiquing Brandom on forgiveness might show that Brandom and Benjamin are not in agreement about this particular point. [https://philosophynow.org/issues/140/Hegel_On_The_Future_Hegel_In_The_Future](https://philosophynow.org/issues/140/Hegel_On_The_Future_Hegel_In_The_Future)

[7](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-anchor-7-48019475)

Mark V. Alznauer, "Hegel, Brandom, and Semantic Descent: Comments on _A Spirit of Trust_ by Robert B. Brandom (Harvard, 2019)," _Existenz_ 15/1 (2020), 42.

[8](https://critiqueofpuretakes.substack.com/p/the-view-from-nowhere#footnote-anchor-8-48019475)

[https://mediationsjournal.org/articles/back-to-hegel](https://mediationsjournal.org/articles/back-to-hegel)
