+++
title = "Reading List"

description = "Reading list"

# Used to sort pages by "date", "update_date", "title", "title_bytes", "weight", "slug" or "none". See below for more information.
sort_by = "none"

# If set to "true", the section homepage is rendered.
# Useful when the section is used to organize pages (not used directly).
render = true

template = "page.html"
# Your own data.
[extra]
+++

## Reading List

What I am reading or have read.

### 2025

| Title                                             |      Author(s)      |         Status          |
| :------------------------------------------------ | :-----------------: | :---------------------: |
| Principles of Economics                           |   Gregory Mankiw    |         Reading         |
| The Will to Power                                 |      Nietzsche      |         Reading         |
| Literary Theory for Robots                        |   Dennis Ye Tenen   |         Reading         |
| The Artist's Way                                  |    Julia Cameron    |         Reading         |
| Poor Charlie's Almanack                           |   Charlie Munger    |      Partial, Done      |
| On the Uses and Disadvantages of History for Life |      Nietzsche      |          Done           |
| The Lily of the Field and the Bird of the Air     |     Kierkegaard     |          Done           |
| Fear and Trembling                                |     Kierkegaard     |          Done           |
| On Truth and Untruth: Selected Writings           |      Nietzsche      |      Partial, Done      |
| Chip War                                          |    Chris Miller     |          Done           |
| The Iliad                                         | Homer (tr. Wilson)  |         Paused          |
| Notes from the Undeground                         |  Fyodor Dostoevsky  |      Partial, Done      |
| Essays and Lectures                               | Ralph Waldo Emerson | Partial, Paused or Done |
| The Witness of Poetry                             |   Czesław Miłosz    |          Done           |
| The Meaningless of Meaning                        |         LRB         |          Done           |
| The Worldly Philosophers                          |     Heilbroner      |          Done           |

### 2024

| Title                                              |               Author(s)                |    Status     |
| :------------------------------------------------- | :------------------------------------: | :-----------: |
| Taking Back Control?                               |            Wolfgang Streeck            | Partial, Done |
| Lucky Jim                                          |             Kingsley Amis              |     Done      |
| Classics of American Literature                    |             D.H. Lawrence              |     Done      |
| What Does Israel Fear From Palestine?              |             Raja Shehadeh              |     Done      |
| The Daemon Knows                                   |              Harold Bloom              | Partial, Done |
| Say It Well                                        |             Terry Szuplat              |     Done      |
| The Practice of Adaptive Leadership                |          Ronald Heifetz, etc           |     Done      |
| Modernism as a Philosophical Problem               |             Robert Pippin              |     Done      |
| The Politics of Being                              |             Richard Wolin              | Partial, Done |
| Leaves of Grass                                    |              Walt Whitman              |     Done      |
| Democracy in America                               |         Alexis de Tocqueville          |     Done      |
| Autumn 2024                                        |           Liberties Journal            |     Done      |
| American Nietzsche                                 |       Jennifer Ratner-Rosenhagen       |     Done      |
| War                                                |              Bob Woodward              |     Done      |
| Things Hidden Since the Foundation of the World    |              René Girard               | Partial, Done |
| The Seduction of Unreason                          |             Richard Wolin              |     Done      |
| Leadership Without Easy Answers                    |             Ronald Heifetz             |     Done      |
| All Desire is a Desire for Being                   |              René Girard               | Partial, Done |
| Wherever You Go, There You Are                     |             Jon Kabat-Zinn             |     Done      |
| Violence and the Sacred                            |              René Girard               |     Done      |
| The Systems Bible                                  |               John Gall                |     Done      |
| The Once and Future Liberal                        |               Mark Lilla               |     Done      |
| The Reckless Mind                                  |               Mark Lilla               |     Done      |
| The Shipwrecked Mind                               |               Mark Lilla               |     Done      |
| Influence                                          |            Robert Cialdini             | Partial, Done |
| Building Microservices                             |               Sam Newman               | Partial, Done |
| The Cynic Philosophers: From Diogenes to Julian    |           ed. Robert Dobbin            | Partial, Done |
| A Short History of Decay                           |              E. M. Cioran              |     Done      |
| The Conservative Revolution in the Weimar Republic |              Roger Woods               | Done, Partial |
| Cosmic Connections                                 |             Charles Taylor             | Done, Partial |
| When the Clock Broke                               |               John Ganz                |     Done      |
| Turn the Ship Around!                              |            L. David Marquet            |     Done      |
| Sacrifice and Modern Thought                       | ed. Julia Meszaros, Johannes Zachhuber | Done, Partial |
| On Being a Pagan                                   |            Alain de Benoist            |     Done      |
| The Sacred Conspiracy                              |            Georges Bataille            |     Done      |
| The Metaphysics of German Idealism                 |            Martin Heidegger            |     Done      |
| Pathmarks                                          |            Martin Heidegger            |     Done      |
| Identity and Difference                            |            Martin Heidegger            |     Done      |
| Scaling People                                     |         Claire Hughes Johnson          |     Done      |
| The Heidegger Controversy                          |           ed. Richard Wolin            |     Done      |
| Parmenides                                         |            Martin Heidegger            |     Done      |
| Four Seminars                                      |            Martin Heidegger            |     Done      |
| Poetry, Language, Thought                          |            Martin Heidegger            |     Done      |
| Early Greek Thinking                               |            Martin Heidegger            |     Done      |
| Martin Heidegger's Changing Destinies              |            Guillaume Payen             |     Done      |
| Heraclitus                                         |            Martin Heidegger            |     Done      |
| The Art and Thought of Heraclitus                  |            Charles H. Kahn             |     Done      |
| Basic Writings                                     |            Martin Heidegger            |     Done      |
| American Philosophy: A Love Story                  |               John Kaag                |     Done      |
| Heraclitus Seminar                                 |      Martin Heidegger, Eugen Fink      |     Done      |
| A History of Philosophy in America 1720-2000       |             Brucke Kuklick             |     Done      |
| Primed to Perform                                  |      Lindsay McGregor, Neel Doshi      |     Done      |
| Twilight of the Idols and The Anti-Christ          |          Friedrich Nietzsche           |     Done      |
| Thus Spake Zarathustra                             |          Friedrich Nietzsche           |     Done      |
| The Elements of Style                              |            Strunk and White            |     Done      |
| On Writing Well                                    |             William Zinser             |     Done      |
| A Philosophy of Walking                            |             Frédéric Gros              |     Done      |
| Traumatic Neuroses of War                          |             Abram Kardiner             |     Done      |
| The Culmination                                    |             Robert Pippin              |     Done      |
| Being and Time                                     |            Martin Heidegger            |     Done      |
| A Hitch in Time                                    |          Christopher Hitchens          |     Done      |
| The Cambridge Companion to Heidegger               |      Charles B. Guignon (Editor)       |     Done      |
| Parapraxis Mag, Vol. 2                             | Hannah Zeavin, Alex Colston (Editors)  |     Done      |
| Essays and Reviews, 1959-2002                      |            Bernard Williams            |     Done      |
| Proustian Uncertanties                             |            Saul Friedlander            |     Done      |

### 2023

| Title                                                      |               Author(s)               |    Status     |
| :--------------------------------------------------------- | :-----------------------------------: | :-----------: |
| The Basic Writings of Nietzsche                            |               Nietzsche               |     Done      |
| Learning Domain Driven Design                              |             Vlad Khononov             |     Done      |
| From Monolith to Microservices                             |              Sam Newman               |     Done      |
| Palestinian Walks                                          |            Rajah Shehadeh             |     Done      |
| Letters to my Palestinian Neighbors                        |          Yossi Klein Halevi           |     Done      |
| Philosophical Analysis in the Twentieth Century            |             Scott Soames              |     Done      |
| Moby Dick                                                  |            Herman Melville            |     Done      |
| Parapraxis Mag, Vol. 1                                     | Hannah Zeavin, Alex Colston (Editors) |     Done      |
| The Times                                                  |            Adam Nagourney             |     Done      |
| Analytic Philosophy, An Anthology                          |    David Sosa, Aloysius Martininch    |     Done      |
| Post-Analytic Philosophy                                   |      John Raichman, Cornel West       |     Done      |
| Readings in Classical Chinese Philosophy                   |   Philop Ivanhoe, Bryan van Norden    |     Done      |
| Tao Te Ching                                               |           Lao Tzu (Hackett)           |     Done      |
| Philosophy in History                                      |      Rorty, Schneewind, Skinner       |     Done      |
| The Shadow of God                                          |             Michael Rosen             |     Done      |
| The Complete Stories                                       |              Franz Kafka              |     Done      |
| Russel, Idealism, and the Emergence of Analytic Philosophy |             Peter Hylton              |     Done      |
| Orientalism                                                |              Edward Said              |     Done      |
| Team of Teams                                              |          Stanley McChrystal           |     Done      |
| Analysis of Analysis                                       |     Tom Raysmith, Michael Beaney      |     Done      |
| A Companion to Analytic Philosophy                         |    David Sosa, Aloysius Martininch    | Partial, Done |
