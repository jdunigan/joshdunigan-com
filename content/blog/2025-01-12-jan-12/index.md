+++
title = "January 12"

descripion = "January 12 blog entry"
+++

<img src="imgs/IMG_5340.jpeg" alt="Aeon Bookstore" style="width:500px;"/>

Today, Mel and I got coffee in the morning with our friend Duncan. Afterwards, we wanted to go to a bookstore. Turns out, none of the bookstores in the area opened even though it was 11:30. So we walked a bit elsewhere and waited for another one that opens at noon. We all could probably spend all of our money on books.

---

I was hoping to work more on my essay over the holidays, but I came down with a bad flu for a few weeks, and still have a cough. I did not get to read much either.

I wanted to get an essay published in 2024, but never found much time to write. It was a really busy year. Work got busy. Mel graduated. We moved. We got married. We got a puppy. Mel got a job.

So this year, I want to get to writing more. Things are bit slower, so I hope to get to writing more now. Last year's goal will be this year. Tentative essay will be called something like "Meaning and Intelligibility", on two strands of philosophy. It is loosely about Robert Pippin and his new book, but also about a comment he made that mirrors my reading in philosophy in the last 6 years or so. There is a type of philosophy that has its archetype's in Kant and Hegel, and another that has its archetypes in Nietzsche and Heidegger.

---

I deleted twitter awhile ago and soon after all social media (just instagram at that point). I got a bunch of subscriptions to places. I have tried out the New Left Review, NYRB, LRB, Jacobin, The Point Mag, The New Yorker, Liberties, NYTimes, WSJ, and Parapraxis. The ones that I find the most valuable are NYRB, LRB, Liberties, and the NYTimes. Print media is great, and they all have great online versions. The online versions have more features such as podcasts and an archive as well, which is valuable especially for places like the NYRB and LRB. They really are worth the money. I also like some of the stuff Compact puts out, but won't subscribe because I imagine some of the crank pieces will annoy me too much.

I have found that it is really not that bad to live without social media. It is very overrated. The cycles of news and discourse that go to social media are never better, but just different. The only thing that is missing is discovery is a bit harder for online content without twitter, which was my favorite part before. My bookmarks were loaded with all sorts of essays and links and such.

Substack helps a bit, although the Notes part is horrible. I deleted mine and recently rejoined to follow some economics folks. But I am iffy on the platform itself. The product is still new and prices are getting figured out. A few substacks seem to dominate on there like any platform, Youtube, etc. Some substacks like Doomberg actually seem almost worth the money, but then you realize that it is only about 6 (? or so ?) articles a month and the minimum subscription is 300 a year. I am sure it is actually worth the cost for the content, good economics and energy reporting from veterans of the industry. However, the amount of "product/service" that you get per dollar is much less than say, the NYRB or the NYTimes.

I think the model of the NYTimes, LRB, and NYRB, with print/digital plus things like podcasts and such are a good model. They also create an institution, can publish at a more frequent rate. If Doomberg + some other folks created a Doomberg Magazine, that gave access to some more articles, I would be interested at that price.

Doomberg may not be the best example, I imagine the folks who really _need_ that info for their career or studies in economics or politics or business can easily afford the cost and it is worth it. But even niche magazines can provide similar amount and quality of reporting and essays for their niche while being much less.

Maybe there will be competition, or maybe this new era of platforms allows people to say, hey, I am not writing this for more less than X dollars. People come direct to me, no (or little to no on substack) middle man. My niche knowledge is worth X a year. I used to make about that in the industry or part of a collective, but I want to do my own thing. That's fine I guess.

But too much of that makes being a general user of substack not worth it for most people I imagine.

