+++
title = "Free reading"

descripion = "On reading outside of academia"
+++

> The learned and the studious of thought have no monopoly of wisdom. Their violence of direction in some degree disqualifies them to think truly. &mdash; Emerson, *The Over-Soul*

During the height of COVID, as a new college grad, quarantining in my parent's basement, I read. I read and read and read. In between reading I slept and worked.

I graduated in philosophy, but that was a side quest. Most of my time was studying computer science and working towards a career as a software engineer, a data plumber more like. We call ourselves software engineers and take theory classes to justify our superiority to bootcampers and self-taught programmers. There is a difference at the highest level, where theory matters, but the vast majority of us are like data plumbers, or if we get really good, we can graduate to data plumbing at the scale of a highrise or even a city.

Being a data plumber for my career filled me with angst, even during college. Even if I changed career paths, I would do it after paying off my loans. I may as well be a little pragmatic and pay my loans off and build some savings with the high data plumber income.

Philosophy was the only time I truly had *fun* at college. It was the only time it felt like the promise of intellectual or spiritual transformation was possible.

Most of the modern day university is a poor matching function for white collar jobs, hence the somewhat rational right-wing, populist critique that we should just abandon all that stuff about Plato, Homer, poetry, history, and especially the "woke shit", and make them into white collar trade schools. I get it. Why [pretend](https://libertiesjournal.com/articles/why-college-or-what-have-we-done/) there are other intrinsic values to university and not just be honest that it is a weed out mechanism, a fancy piece of paper mill, for our white collar elites?

I agree with this sentiment. It would be absurd to take out 100k in debt to read *The Critique of Pure Reason*, and the university system that promotes this fantasy is fiscally irresponsible.

But there should be something more to life. There has to be.

The angst turned into dread during COVID. The world and my work felt like a low point. Work felt especially meaningless compared to what was happening in the world.

I kept up my philosophical studies from college. I dove deeper into Kant. After Kant, I moved onto Hegel. I spent almost two and half years Kant and Hegel. This year, 2024, I dove into Nietzsche and Heidegger. I also have taken many a detour, reading some literature, poetry, history, politics, etc.

My reading interests expand and contract. Sometimes I go in-depth, sometimes breadth. There is no set bibliography I have to read for a dissertation.

I think about the way in which I can just read and read. There are no outcomes I have to make true. There is no expected output.

In academia, you have to justify your career. This is good. Academia, especially taxpayer funded parts, have a role in educating our citizens to be successful in many ways and provide vital research so that our democracy can use that research to make decisions or improve society. [When academia abandons that](https://libertiesjournal.com/articles/killing-history/), it loses its justification.

But academia is also supposed to be free. A place for free inquiry. Free debate. Free studying.

An undergrad cannot just study what they want, they need to have a major or minor. A grad student cannot just read and write something, they need a dissertation. Professors have to justify themselves as well, whether its for expertise in teaching, or justifying their tenure-track position.

This is where academia becomes a business. Or at least, needs strong organizational skills that the business world runs on. At work, you have to justify what you are working on, show progress, show some sort of "fit" to the market and customers. You need to be able to plan milestones.

Changing course often can be great in business. Better to try something out for a month than be like [Apple](https://en.wikipedia.org/wiki/Apple_car_project). It is possible that even Apple did not waste 10 years and 10 billion dollars if they learned enough to justify it, or produced other things to justify the investment.

Just like in business, however, you cannot diverge too great from a plan. You cannot plan a dissertation on, say, Plato, but spend a year reading early American political thought or the Bible. If you do, you will probably be forced to stop or create a chimera-like dissertation, with some weird marriage of your reading digressions.

Or maybe your digression is still within the realm of relevance, but how much can you digress? A month? A year? There are limits.

All fields in academia are going through this, the incentives of academia are becoming more like a business. It was not even 100 years ago that professors in philosophy could publish maybe one or two papers for their early and middle career and be considered distinguished and experts. Now, undergrads are getting published. STEM fields have an even worse problem, especially machine learning, where publishing is so frequent that there is just too much noise to wade through.

There are benefits of academia, you sacrifice some freedom for the institution, access to others, and hey, its your job to research and teach.

But I like where I ended up. I can meander my way through books as I feel.
